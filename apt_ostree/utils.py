"""
Copyright (c) 2023 Wind River Systems, Inc.

SPDX-License-Identifier: Apache-2.0

"""

import logging
import subprocess
import sys

LOG = logging.getLogger(__name__)


def run_command(args, check=True, log_func=LOG.debug, **kwargs):
    """Run a command in a shell."""
    if log_func:
        log_func("Running %s" % args)

    outputs = ""
    sp = subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        stdin=subprocess.PIPE,
        universal_newlines=True,
        **kwargs,
    )
    while True:
        output = sp.stdout.readline()
        if output:
            if log_func:
                log_func(output.rstrip("\n"))
            outputs += output
        if sp.poll() is not None:
            break

    # Read the remaining logs from stdout after process terminates
    output = sp.stdout.read()
    if output:
        if log_func:
            log_func(output.rstrip("\n"))
        outputs += output

    ret = sp.poll()

    if ret != 0:
        if check:
            LOG.error(f"Failed to run command {args}\n{outputs}")
            sys.exit(1)

    return ret, outputs.rstrip("\n")


def run_sandbox_command(args, rootfs, **kwargs):
    """Run a shell wrapped with bwrap."""
    cmd = [
        "bwrap",
        "--proc",
        "/proc",
        "--dev",
        "/dev",
        "--bind",
        "/run",
        "/run",
        "--bind",
        "/tmp",
        "/tmp",
        "--bind",
        f"{rootfs}/boot",
        "/boot",
        "--bind",
        f"{rootfs}/usr",
        "/usr",
        "--bind",
        f"{rootfs}/etc",
        "/etc",
        "--bind",
        f"{rootfs}/var",
        "/var",
        "--bind",
        f"{rootfs}/opt",
        "/opt",
        "--symlink",
        "/usr/lib",
        "/lib",
        "--symlink",
        "/usr/lib64",
        "/lib64",
        "--symlink",
        "/usr/bin",
        "/bin",
        "--symlink",
        "/usr/sbin",
        "/sbin",
        "--ro-bind",
        "/etc/resolv.conf",
        "/etc/resolv.conf",
        "--share-net",
        "--die-with-parent",
        "--chdir",
        "/",
    ]
    cmd += args

    return run_command(cmd, **kwargs)
