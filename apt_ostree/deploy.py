"""
Copyright (c) 2023 Wind River Systems, Inc.

SPDX-License-Identifier: Apache-2.0

"""

import logging
import os
import shutil
import subprocess
import sys
import textwrap

from rich.console import Console

from apt_ostree import utils
from apt_ostree.ostree import Ostree

APT_CONF = "etc/apt/sources.list.d/apt-ostree.list"


class Deploy:
    def __init__(self, state):
        self.console = Console()
        self.logging = logging.getLogger(__name__)
        self.state = state
        self.ostree = Ostree(self.state)

        self.workspace = self.state.workspace
        self.workdir = self.state.workspace.joinpath("deployment")
        self.workdir.mkdir(parents=True, exist_ok=True)
        self.rootfs = None

    def prestaging(self, rootfs, feed=None):
        """Pre stage steps."""
        if not rootfs.exists():
            self.logging.error("rootfs not found: {rootfs}")
            sys.exit(1)

        self.logging.info("Running pre-staging steps.")
        with self.console.status("Running pre-staging steps."):
            fd = os.open(rootfs, os.O_DIRECTORY)
            # Ensure that we correct permissions
            os.chown(rootfs, 0, 0)
            if not rootfs.joinpath("etc").exists():
                self.logging.info("Converting /usr/etc -> /etc.")
                os.symlink("usr/etc", "etc", dir_fd=fd)
                os.chown(rootfs.joinpath("usr/etc"), 0, 0)
            if rootfs.joinpath("usr/rootdirs/var").exists():
                # /var is suppose to hold the state of the running
                # system.
                self.logging.info("Moving /usr/rootdirs/var -> /var.")
                os.rmdir("var", dir_fd=fd)
                shutil.move(
                    f"{rootfs.joinpath('usr/rootdirs/var')}",
                    f"{rootfs.joinpath('var')}",
                )
                self.logging.info("Configuring package manager.")
                os.symlink("../../usr/share/dpkg/database", "var/lib/dpkg", dir_fd=fd)
            if feed:
                self.logging.info("Configuring temporary package feed.")
                self.apt_conf = rootfs.joinpath(APT_CONF)
                feed = feed.replace('"', "")
                self.apt_conf.write_text(
                    textwrap.dedent(
                        f"""\
                    deb [trusted=yes] {feed}
                    """
                    )
                )

    def poststaging(self, rootfs):
        """Post staging steps."""
        if not rootfs.exists():
            self.logging.error("rootfs not found: {rootfs}")
            sys.exit(1)

        self.logging.info("Running post deploy steps.")
        fd = os.open(rootfs, os.O_DIRECTORY)

        if os.path.exists(rootfs.joinpath("etc")):
            self.logging.info("Converting /etc -> /usr/etc.")
            os.unlink("etc", dir_fd=fd)

        os.chown(rootfs.joinpath("usr/etc"), 0, 0)
        if rootfs.joinpath("var/lib/dpkg").exists():
            self.logging.info("Cleanup package manager.")
            os.unlink("var/lib/dpkg", dir_fd=fd)

            # /var is stateless so remove everything that
            # might be lying around.
            self.logging.info("Moving /var -> /usr/rootdirs/var.")
            shutil.move(
                f"{rootfs.joinpath('var')}", f"{rootfs.joinpath('usr/rootdirs/var')}"
            )

            os.mkdir("var", dir_fd=fd, mode=0o755)
            os.chown(rootfs.joinpath("usr/rootdirs/var"), 0, 0)

        apt_conf = rootfs.joinpath(APT_CONF)
        if apt_conf.exists():
            apt_conf.unlink()

    def cleanup(self, rootfs):
        """Remove workspace directories to save space."""
        self.logging.info("Cleaning up.")
        if os.path.exists(rootfs):
            shutil.rmtree(rootfs)

    def get_sysroot(self, branch=None):
        """Checkout the commit to the specified directory."""
        if branch is None:
            branch = self.ostree.get_branch()
        rev = self.ostree.ostree_ref(branch)
        self.logging.info(f"Checking out {rev[:10]} from {branch}.")
        with self.console.status(f"Checking out {rev[:10]}..."):
            self.workdir = self.workdir.joinpath(branch)
            self.workdir.mkdir(parents=True, exist_ok=True)
            self.rootfs = self.workdir.joinpath(rev)
            if self.rootfs.exists():
                shutil.rmtree(self.rootfs)
            self.ostree.ostree_checkout(branch, self.rootfs)
        return self.rootfs

    def _get_deploy_rootfs(self, branch):
        cmd = f"ostree show {self.state.branch}"
        ret, out = utils.run_command(cmd, shell=True)
        for line in out.splitlines():
            if line.startswith("commit "):
                cmd = f"ls -d /ostree/deploy/*/deploy/{line.split()[1]}.? -d| sort"
                ret, out = utils.run_command(cmd, shell=True)
                for d in out.splitlines():
                    deploy_rootfs = d

                self.logging.info(f"Deploy rootfs {deploy_rootfs}")
                return deploy_rootfs

        self.logging.error(f"Deploy rootfs not found")
        return ""

    def deploy(self, reboot):
        """Run ostree admin deploy."""
        ref = self.ostree.ostree_ref(self.state.branch)
        if not ref:
            self.logging.error(f"Unable to find branch: {self.state.branch}.")
            sys.exit(1)

        ret, _ = utils.run_command(["ostree", "admin", "deploy", self.state.branch])
        if ret != 0:
            self.logging.error("Failed to deploy.")
            sys.exit(1)

        deploy_rootfs = self._get_deploy_rootfs(self.state.branch)
        if deploy_rootfs:
            self.logging.info(f"Updating /var")
            var_from = os.path.join(deploy_rootfs, "usr/rootdirs/var")

            cmd = "ls /ostree/deploy/*/var -d"
            ret, var_to = utils.run_command(cmd, shell=True)

            self.logging.debug(f"Copying {var_from} -> {var_to}")
            cmd = f"cp -af {var_from}/* {var_to}"
            utils.run_command(cmd, shell=True)

        if reboot:
            self.logging.info("Rebooting now.")
            ret, _ = utils.run_command(["shutdown", "-r", "now"])
            if ret != 0:
                self.logging.error("Failed to reboot.")
                sys.exit(1)
        else:
            self.logging.info("Please reboot for the changes to take affect.")
