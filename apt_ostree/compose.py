"""
Copyright (c) 2023 Wind River Systems, Inc.

SPDX-License-Identifier: Apache-2.0

"""

import logging
import pathlib
import shutil
import sys

from rich.console import Console

from apt_ostree.deploy import Deploy
from apt_ostree.ostree import Ostree
from apt_ostree.repo import Repo


class Compose:
    def __init__(self, state):
        self.logging = logging.getLogger(__name__)
        self.state = state
        self.ostree = Ostree(self.state)
        self.repo = Repo(self.state)
        self.deploy = Deploy(self.state)
        self.console = Console()

        self.workspace = self.state.workspace
        self.workdir = self.state.workspace.joinpath("deployment")
        self.workdir.mkdir(parents=True, exist_ok=True)
        self.rootfs = None

    def rollback(self, commit):
        """Rolling back to a previous commit."""
        self.logging.info(f"Rollback back to {commit}.")
        self.ostree.ostree_rollback(commit)

    def enablerepo(self):
        """Enable Debian package feed."""
        try:
            self.repo.add_repo()
        except Exception as e:
            self.logging.error(f"Failed to add repo: {e}")
            sys.exit(1)

    def disablerepo(self):
        """Disable Debian package feed."""
        self.repo.disable_repo()
