"""
Copyright (c) 2023 Wind River Systems, Inc.

SPDX-License-Identifier: Apache-2.0
"""

import logging
import os
import sys
import time

from rich.console import Console
from rich.logging import RichHandler
from systemd.journal import JournalHandler


def setup_log(debug=False, workdir=None):
    level = logging.DEBUG if debug else logging.INFO
    fmt = "%(message)s"

    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.NOTSET)

    journald_handler = JournalHandler(SYSLOG_IDENTIFIER="apt-ostree")
    journald_handler.setLevel(level)
    journald_handler.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
    rootLogger.addHandler(journald_handler)

    console = Console(color_system=None)

    rich_handler = RichHandler(
        show_path=False, show_time=False, show_level=False, console=console
    )
    rich_handler.setLevel(level)
    rich_handler.setFormatter(logging.Formatter(fmt))
    rootLogger.addHandler(rich_handler)

    # Save log to file
    set_logger_file(rootLogger, workdir)

    return rootLogger


def set_logger_file(logger, workdir=None):
    if workdir is None:
        return

    for log_path in ["/var/log", workdir]:
        if os.access(log_path, os.W_OK):
            break
    os.makedirs(log_path, exist_ok=True)

    log = os.path.join(log_path, "apt-ostree.term-{0}.log".format(int(time.time())))

    # Create log symlink if not in unit test
    if "click.testing" not in sys.modules.keys():
        log_symlink = os.path.join(log_path, "apt-ostree.term.log")
        if os.path.islink(log_symlink):
            os.remove(log_symlink)
        os.symlink(os.path.basename(log), log_symlink)

    FORMAT = "[%(levelname)s] %(message)s"
    fh = logging.FileHandler(log)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter(FORMAT))
    logger.addHandler(fh)
